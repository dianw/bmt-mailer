var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var kontakSchema = new Schema({
	kontak_id:Number,
	nama:String,
	email:String,
	telp:String,
	alamat:String,
	group:String
});

var Kontak = mongoose.model('Kontak', kontakSchema);
module.exports.Kontak = Kontak;